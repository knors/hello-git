﻿namespace Calculator
{
    public class Besmart
    {
        public double A { get; set; }
        public double B { get; set; }
        public double C { get; set; }
        public double X1 { get; set; }
        public double X2 { get; set; }

        public double CalculateD(double a, double b, double c)
        {
            var d = b * b - 4 * a * c;
            return d;
        }
    }
}
